# Ứng dụng Quản Lý Chi Tiêu

Ứng dụng cho phép quản lý chi tiêu trong phòng.

## Nội Dung

 1. [Chức năng](#chức-năng)

    1.1	Đăng ký và Đăng nhập

    1.2	Tạo nhóm

    1.3	Quản lý thông tin khoản chi nhóm

    1.4	Tạo khoản chi nhóm

    1.5	Quản lý thông tin tổng quan của nhóm

    1.6	Thống kê khoản chi của nhóm

    1.7	Chỉnh sửa thông tin nhóm

 2. [Yêu cầu thiết bị](#yêu-cầu-thiết-bị)

 3. [Thư viện và công nghệ](#thư-viện-và-công-nghệ)

 4. [Tác giả](#tác-giả)

## Chức năng

    ###	Đăng ký và Đăng nhập

    ###	Tạo nhóm

    ###	Quản lý thông tin khoản chi nhóm

    ###	Tạo khoản chi nhóm

    ###	Quản lý thông tin tổng quan của nhóm

    ###	Thống kê khoản chi của nhóm

    ###	Chỉnh sửa thông tin nhóm

## Yêu cầu thiết bị

- Android:

   + Android studio : 3.5.2

   + Min sdk : 21

   + Target sdk : 29

## Thư viện và công nghệ

- Language : Dart

- FrameWork : Flutter

- Database: firebase

## Tác giả

 Trương Văn Thành - 17521062@gm.uit.edu.vn 

 Nguyễn Đức Phúc - 17520906@gm.uit.edu.vn 
 
 Nguyễn Duy Phước - 17520916@gm.uit.edu.vn 


